CXXFLAGS =	-Wl,--no-as-needed -DDEBUG -g -std=gnu++17 -pthread
#CXXFLAGS =	-Wl,--no-as-needed -DDEBUG -g -std=gnu++17 -pthread -fsanitize=address
#CXXFLAGS =	-O2 -Wl,--no-as-needed -DDEBUG -g -std=c++11 -pthread

#OBJS =		cpuAffinity.o
OBJS =		appearOnce.o

#LIBS = -fsanitize=address -static-libasan

#TARGET =	MatchEngineData/AppCpp
TARGET =	AppCpp

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS) 

all:	$(TARGET)

clean:
	rm -f  *.o $(OBJS) $(TARGET)
