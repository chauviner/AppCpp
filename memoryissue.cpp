#include <iostream>
#include <cstring>

int main(int argc, const char *argv[]) {
    char *cstr = new char[11];
    strcpy(cstr, "Hello World");
    std::cout << cstr << std::endl;

    delete[] cstr;
    return 0;
}
